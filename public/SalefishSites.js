salefishsites_callback({
"type":"ProjectCollection",
"projects":[
{"properties":{"prjno":"A0034","latitude":45.494493,"longitude":-73.573719,"prjname":"YUL","municipality":"Montreal, QC","builder":"Brivia Group"}},
{"properties":{"prjno":"A0038","latitude":44.025613,"longitude":-79.429408,"prjname":"Aurora Trails","municipality":"Aurora, ON","builder":"Opus Homes"}},
{"properties":{"prjno":"A0043","latitude":43.877880,"longitude":-79.232667,"prjname":"Grand Cornell","municipality":"Markham, ON","builder":"Lindvest"}},
{"properties":{"prjno":"A0048","latitude":44.113469,"longitude":-79.464685,"prjname":"Queens Landing","municipality":"East Gwillimbury, ON","builder":"Minto Communities"}},
{"properties":{"prjno":"A0053","latitude":44.017134,"longitude":-79.424182,"prjname":"Aurora - phase 2","municipality":"Aurora, ON","builder":"Opus Homes"}},
{"properties":{"prjno":"A0057","latitude":43.702493,"longitude":-79.850529,"prjname":"Encore Brampton","municipality":"Brampton, ON","builder":"Gold Park Homes"}},
{"properties":{"prjno":"A0060","latitude":43.978935,"longitude":-79.260710,"prjname":"Fairgate Meadows","municipality":"Stouffville, ON","builder":"Fairgate Homes"}},
{"properties":{"prjno":"A0115","latitude":29.431746,"longitude":-98.489114,"prjname":"Arts Condo","municipality":"Houston, Texas","builder":"DC Partners"}},
{"properties":{"prjno":"A0122","latitude":45.494822,"longitude":-73.573342,"prjname":"YUL Building 2","municipality":"Montreal, QC","builder":"Brivia Group"}},
{"properties":{"prjno":"A0129","latitude":43.897057,"longitude":-78.979101,"prjname":"Whitby Meadows","municipality":"Whitby, ON","builder":"Opus-Deco Homes"}},
{"properties":{"prjno":"A0131","latitude":43.963292,"longitude":-79.408712,"prjname":"Richlands","municipality":"Richmond Hill, ON","builder":"OPUS Homes"}},
{"properties":{"prjno":"A0162","latitude":43.710580,"longitude":-79.846955,"prjname":"Origins","municipality":"Brampton, ON","builder":"OPUS and DECO Homes"}},
{"properties":{"prjno":"Jamaica","latitude":18.397459,"longitude":-77.002610,"prjname":"Edgehill","municipality":"Jamaica","builder":"Dunsire Homes"}}
]
});
